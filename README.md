# Git Training
This is a sample Git repository used in my *You don't know Git* talk.

Feel free to clone the repository and run through the *cheat-sheets*.

## Exercise
The goal of this exercise is to simulate working with remote repositories, making changes, and the process of Pull Requests. To get started, perform the following steps:

1. "Clone" your fork of the repository so you can work on it locally.
    - `git clone git@gitlab.com:state-farm/ut-dallas-acm-kickoff-2022.git`
1. Checkout a branch
    - `git checkout -b perry-branch`
    - `-b` means create a new branch and switch to it. 
1. Append the registry.txt file with your name
1. Add your change
    - `git add registry.txt`
    - `git commit -m "${COMMIT_MESSAGE}"`
1. Publish your changes to your upstream.
    - Currently, all changes are local. In order to changes to reflect, it should be pushed to remote.
    - ` git push --set-upstream origin perry-branch`
1. Open a "Merge Request" on Gitlab to submit your changes to the upstream repository.
1. Merge the changes
1. But what if someone else makes the change first?
1. Need to merge the changes and decide which one to take.
    - `git checkout main`
    - `git pull` 
    - `git checkout perry-merge`
    - `git merge main`
1. Now push up your merged changes
1. Create another MR!
